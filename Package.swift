// swift-tools-version:4.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "JSONServer",
    products: [
	.library(name: "JSONServer", targets: ["JSONServer"])
    ],
    dependencies: [
        .package(url: "https://github.com/vapor/http.git", .exact("3.1.8"))
    ],
    targets: [
        .target(
            name: "JSONServer",
            dependencies: ["HTTP"]),
        .testTarget(
            name: "JSONServerTests",
            dependencies: ["JSONServer"]),
    ]
)
