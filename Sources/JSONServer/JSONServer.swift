import HTTP

/// A server which processes HTTP requests with a JSON body and returns JSON responses to the client app.
public class JSONServer: HTTPServerResponder {
  
  /// The signature for a standard handler. This is called on a concurrent global queue but expects a response when the closure ends.
  public typealias Handler<Req: Decodable, Res: Encodable> = (Req) throws -> Res
  
  /// The signature for a handler which allows additional async processing after the closure ends. The responsibility is on the developer to call one of the `res()` or `error()` callbacks at some point.
  public typealias AsyncHandler<Req: Decodable, Res: Encodable> = (
    _ req: Req,
    _ res: @escaping (Res) -> Void,
    _ error: @escaping (Error) -> Void)
    throws -> Void
  
  /// A base-level handler (world of Data). How all handlers are stored.
  private typealias BaseHandler = (
    Data,
    @escaping (Data) -> Void)
    throws -> Void
  
  /// All the handlers
  private var handlers = [String: BaseHandler]()
  
  /// Adds a synchronous handler for the given endpoint. This handler will be called on a concurrent global queue but expects a response when the closure ends.
  public func add<Req: Decodable, Res: Encodable>(
    _ endpoint: String,
    _ handler: @escaping Handler<Req, Res>) {
    
    // set BaseHandler (world of Data)
    handlers[endpoint] = { reqData, resData in
      
      // that is lifted up into a synchronous Handler (world of Types)
      do {
        let req = try decoder.decode(Req.self, from: reqData)
        let encodable = try handler(req)
        let data = try encoder.encode(encodable)
        resData(data)
      } catch {
        resData(errorData(error))
      }
    }
  }
  
  /// Adds an asynchronous handler for the given endpoint which allows additional async processing after the closure ends. The responsibility is on the developer to call one of the `res()` or `error()` callbacks at some point.
  public func add<Req: Decodable, Res: Encodable>(
    _ endpoint: String,
    _ typedHandler: @escaping AsyncHandler<Req, Res>) {
    
    // set BaseHandler (world of Data)
    handlers[endpoint] = { reqData, resData in
      
      // that is lifted up into an AsyncHandler (world of Types)
      try typedHandler(
        
        // AsyncHandler 1st param (Req)
        decoder.decode(Req.self, from: reqData),
        
        // AsyncHandler 2nd param (Res Func)
        {
          do {
            let data = try encoder.encode($0)
            resData(data)
          } catch {
            resData(errorData(error))
          }
      },
        
        // AsyncHandler 3rd param (Error Func)
        {
          resData(errorData($0))
      }
      )
    }
  }
  
  // Errors
  enum Err: Error {
    case endpointNotFound(String)
  }
  
  /// Creates and returns a new JSONServer.
  public init() {}
  
  /// Starts the server on the given port.
  public func start(
    port: Int = 8080) {
      do {
    
        let group = MultiThreadedEventLoopGroup(numberOfThreads: System.coreCount)
        defer { try! group.syncShutdownGracefully() }
        let server = try HTTPServer.start(
          hostname: "0.0.0.0",
          port: port,
          responder: self,
          maxBodySize: 10000000,
          on: group
          ).wait()
        try server.onClose.wait()
      } catch {
        fatalError("\(error)")
      }
  }
  
  /// Returns a response for the given request by looking up and running a handler, then calling `succeed()` on a promise created on the given worker. Required by HTTPServerResponder.
  public func respond(
    to req: HTTPRequest,
    on worker: Worker)
    -> EventLoopFuture<HTTPResponse> {
      
      let endpoint = req.url.path
      let p = worker.eventLoop.newPromise(HTTPResponse.self)
      DispatchQueue.global(qos: .userInteractive).async {
        guard let handler = self.handlers[endpoint] else {
          p.succeed(["error": "Endpoint not found: \(endpoint)"])
          return
        }
        do {
          try handler(
            req.body.data ?? Data(),
            {
              if $0.count == 0 {
                p.succeed(["ok": true])
                return
              }
              let httpResponse = HTTPResponse(
                status: .ok,
                headers: responseHeaders,
                body: HTTPBody(data: $0))
              p.succeed(result: httpResponse)
          })
        } catch {
          p.succeed(["error": "\(error)"])
        }
      }
      return p.futureResult
  }
}

// MARK: - Helpers

extension Promise where T == HTTPResponse {
  /// Causes this promise to respond to the client with the given result converted to an HTTPResponse.
  func succeed<E : Encodable>(_ result: E) {
    do {
      let responseData = try encoder.encode(result)
      let httpResponse = HTTPResponse(
        status: .ok,
        headers: responseHeaders,
        body: HTTPBody(data: responseData))
      succeed(result: httpResponse)
    } catch {
      let response: [String: String] = ["error": "\(error)"]
      let responseData = try! encoder.encode(response)
      let httpResponse = HTTPResponse(
        status: .ok,
        headers: responseHeaders,
        body: HTTPBody(data: responseData))
      succeed(result: httpResponse)
    }
  }
}

extension Request {
  /// Decodes and returns the given type from the body of this request
  func decode<T: Decodable>(_ type: T.Type) throws -> T {
    return try decoder.decode(type, from: body)
  }
}

/// A request from a client that gets passed into handlers. This abstracts away the underlying request model used by the HTTP library.
struct Request {
  let body: Data
}

/// A response to the caller returned as JSON.
enum Response<R> {
  /// An error response: `{"error":"Some error text."}`
  case error(Error)
  /// A response can be any `Codable` value.
  case response(R)
  
  @discardableResult
  func response(handler: (R) -> Void) -> Response {
    switch self {
    case .error(_): break
    case .response(let model): handler(model)
    }
    return self
  }
  
  @discardableResult
  func error(handler: (Error) -> Void) -> Response {
    switch self {
    case .error(let error): handler(error)
    case .response(_): break
    }
    return self
  }
}

/// Creates an error response from the given error.
func errorData(_ error: Error) -> Data {
  return try! encoder.encode(["error": "\(error)"])
}

/// The headers to return in all responses
let responseHeaders = HTTPHeaders([
  ("Content-Type", "application/json")
  ])

/// The encoder for all JSON
private let encoder = JSONEncoder()

/// The decoder for all JSON
private let decoder = JSONDecoder()

