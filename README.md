**JSONServer** is a simple Swift server that speaks Codable JSON.

In your Package.swift:

```
dependencies: [
  .package(url: "https://gitlab.com/joe_lester/JSONServer.git", from: "1.0.0")
], 
```

Simple example:

```swift
import JSONServer

// Define an endpoint handler with whatever `Codable` request/response types you define. 
// They are automatically serialized from/to JSON. You only work with the types.
func login(req: LoginRequest) throws -> LoginResponse {
  return .init(isLoggedIn: true)
}

// Add as many endpoints as you want and start server on port 8080
let server = JSONServer()
server.add("/login", login)
server.start(port: 8080)
```

Async example:

```swift
import JSONServer

// Abbreviate async handler type to "Async"
typealias Async = JSONServer.AsyncHandler

// Define async handler that is generic over the request and the response types.
let loginAsync: Async<LoginRequest, LoginResponse> = { req, res, error in
  // perform some async operation...
  DispatchQueue.global().asyncAfter(deadline: .now() + 3) {
    // respond to client
    res(LoginResponse(isLoggedIn: true))
  }
}

// Add as many endpoints as you want and start server on port 8080
let server = JSONServer()
server.add("/loginAsync", loginAsync)
server.start(port: 8080)
```
