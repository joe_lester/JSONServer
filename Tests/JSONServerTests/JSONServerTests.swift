import XCTest
@testable import JSONServer

final class JSONServerTests: XCTestCase {
  
  // Example request, can be eny Codable type and can be different for each endpoint.
  struct LoginRequest: Codable {
    let publicKey: String
  }
  
  // Example response, can be eny Codable type and can be different for each endpoint.
  struct LoginResponse: Codable {
    let isLoggedIn: Bool
  }
  
  func testSync() {
    
    // Define an endpoint handler with whatever Codable types you want...
    func login(req: LoginRequest) throws -> LoginResponse {
      return .init(isLoggedIn: true)
    }
    
    // Add as many endpoints as you want and start server on port 8080
    let server = JSONServer()
    server.add("/login", login)
  }
  
  func testAsync() {
    
    // Abbreviate async handler type to "Async"
    typealias Async = JSONServer.AsyncHandler
    
    // Define async handler that is generic over the request and the response types.
    let loginAsync: Async<LoginRequest, LoginResponse> = { req, res, error in
      // perform some async operation...
      DispatchQueue.global().asyncAfter(deadline: .now() + 3) {
        // respond to client
        res(LoginResponse(isLoggedIn: true))
      }
    }
    
    // Add as many endpoints as you want and start server on port 8080
    let server = JSONServer()
    server.add("/loginAsync", loginAsync)
  }

  static var allTests = [
    ("testSync", testSync),
  ]
}
