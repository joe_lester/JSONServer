import XCTest

import JSONServerTests

var tests = [XCTestCaseEntry]()
tests += JSONServerTests.allTests()
XCTMain(tests)